# Plotting scripts

This directory contains quite important plotting data depicting the stage
where NA64 (formerly P348) experiment should act.

The exclusion plot is built in coordinates of supposed A' mass and mixing
parameter. Filled regions depicts the already covered areas (by other
experiments) and the lines depicts those that should be provided in near
future or considered by community as a persepctive ones.

For particular details see comments and plot legend.

The `nData/` dir contains actual data.

For NA64 collaborators: please, keep it refreshed as new data outcomes.

## K-factors

For plots relying on `bezier-smooth-excl-inv.dat` data, whether they being
built w/ or w/o K-factors, depends on the column used. The data is taken from
`nData/sng-additional.dat` table and smoothed using bezier splines: in the
`nData/sng-additional.dat` file the column #2 and #3 corrsponds to w/ and w/o
K-facts correspondingly.

## alpha\_D

Plots for Light Thermal Dark Matter strongly rely on the alpha\_D constant that
varies among different publications. The most common values considered are
0.05, 0.1, 0.25, 0.5. One may found this constant being fixed in some
.gpi-scripts (usually called alpha\_D) to somewhat standard for certain plot
type.

# Docker Image

One can get a working environment with all the in a reasonable time with
all the neccessary packages by utilizing the `Dockerfile` in `docker/`
directory.

To build the image, run:

    $ docker build docker/ -t texlive-context:latest

You will need ~1.3 Gb of disk space for this image and 5-10 minutes for it to
be assembled. To run the image as a container use:

    $ docker run --rm --volume <path-to-this-repo>:/var/src \
                      --volume <path-to-build-dir>:/home/collector/build \
                      -ti texlive-context /bin/bash

Note: on SELinux (e.g. RHEL, CentOS, etc) you may need to append
the `/var/src` with `:z` and `/home/collector/build` with `:Z` to get the
proper permissions for these directories.

With volume layout as given in previous command, run

    collector@<container-id> $ cd build
    collector@<container-id> $ make -f /var/src/Makefile

to build all the plots within this repository.

Note, that the above `make` command will produce all the plots and numerical
assets within the repository. If you would like to rather work with a
particular plot, name the taget after `make` command, e.g.:

    collector@<container-id> $ make -f /var/src/Makefile exclusionInvisible.pdf

Of course, one hav to know the name of the target in advance that is best to
find by looking on the .pdf files produced by default.

Produced .pdf files will be abailable, among other stuff, in the host's
directory that you have mentioned in with `<path-to-build-dir>` in
the `docker run` command.

# Troubleshooting

### Running make in a container issues "permission denied error"

If `build/` dir in a `collector`'s directory have `root` as owner, perhaps the
docker daemon was unable to mount your directory (e.g. because of wrong path).
Re-check the path and its permissions.

# Disclaimer on the Structure

This repository suffers from some techical debt due to unexpected growth of the
plots it generates. It is foreseen that this project will eventually undergo
a vast restructuring rpocess when each subset of plot will receive its own
sub-directory. For the time being we collect the information on the original
datasets and plotting scripts related to certain plot or subset of the plot.
See the `nData/_description.txt` for the information collected.

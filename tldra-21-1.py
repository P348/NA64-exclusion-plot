# Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
# Author: Renat R. Dusaev <crank@qcrypt.org>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

manualParameters = {
    'E137' : {
        'centerX' : 20,
        'centerY' : 1e-7,
    },
    'BaBar' : {
        'centerX' : 1e3,
        'centerY' : 1e-7,
        'interpolationMethod' : 'UnivariateSpline',
        'smoothFactor' : .9,
    },
    'NA64' : {
        'centerX' : 5e1,
        'centerY' : 1e-6,
    },
    'MiniBoone' : {
        'centerX' : 2.5e2,
        'centerY' : 1e-6,
        'interpolationMethod' : 'UnivariateSpline',
    },
    'Belle-II' : {
        'centerX' : 2.5e2,
        'centerY' : 1e-3,
    },
    'Landau-Pole' : {
        'interpolationMethod' : None,
    },
    'alpha-EM' : {
        'interpolationMethod' : None,
    },
    'LSND' : {
        'centerX' : 10,
        'centerY' : 1e-5,
        'interpolationMethod' : 'UnivariateSpline',
        'smoothFactor' : .1,
    }
}


#!/usr/bin/env python3

# Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
# Author: Renat R. Dusaev <crank@qcrypt.org>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

from math import sqrt, log
from scipy import interpolate as interMod
import argparse, sys

# from nData/table1_III.txt
tableData = [
    [0.001,      2.15e-9,   100./4000],  #< XXX: fake point introduced for phys. match
    [0.002,      1.3e-9 ,   357./4000],
    [0.005,        4e-10,   556./4000],
    [0.01 ,      1.5e-10,   623./4000],
    [0.02 ,        5e-11,   645./4000],
    [0.03 ,     2.58e-11,   654./4000],
    [0.05 ,      1.1e-11,   279./4000],
    [0.07 ,        5e-12,   125./4000],
    [0.2  ,     5.86e-13,   293./4000],
    [0.5  ,     6.76e-14,   338./4000]
]
eps0dft = 1e-4

def epsilonAt3sigma( eps0, probability, Ne ):
    return sqrt( (2*sqrt(2*log(2))*eps0*eps0)/(Ne*probability) )

def pts_for_ne( Nes, f=None, td=tableData, baseEpsilon=eps0dft ):
    ret = []
    if f:
        f.write( '#me ' + "  ".join(map(str, Nes)) + '\n' )
    for mpPair in td:
        ma, p = mpPair[0], mpPair[1]
        eps = []
        for Ne in Nes:
            eps.append( epsilonAt3sigma( baseEpsilon, p, Ne ) )
        if f:
            f.write( '%e '%ma + " ".join(map(str, eps)) + '\n' )
        ret.append( [ma,] + eps )
    return ret

def extrapolated_pts_for_ne( Nes, outFile ):
    #f.write( '#me ' + "  ".join(map(str, Nes)) + '\n' )
    pts = list([])
    for Ne in Nes:
        #interMod.UnivariateSpline
        interpolatedF = interMod.InterpolatedUnivariateSpline(
                            list(map(lambda p:                           p[0],        tableData)),
                            list(map(lambda p: epsilonAt3sigma( eps0dft, p[1], Ne ) , tableData)),
                          w=list(map(lambda p:                           p[2],        tableData)),
                          k=3,
                          bbox=[1e-3,10]
                          #s=.5
                    )
        k = 0
        for i in range(-3, 1):
            for j in range(1, 10):
                mass = j*10**i
                eps = float(interpolatedF(mass))
                if len(pts) > k:
                    pts[k].append( eps )
                else:
                    pts.append(list([mass, eps]))
                k += 1
    for line in pts:
        for val in line:
            outFile.write('%.3e\t'%val)
        outFile.write('\n')



if "__main__" == __name__:
    parser = argparse.ArgumentParser(description="Expected precision statistics plot.")
    parser.add_argument('-n', '--nIncedentElectrons',
                        type=float,
                        help="Order of number of incident electrons (usually 1e8 ... 1e12 => 9 ... 12)",
                        action='append',
                        nargs='+' )
    parser.add_argument('--plot', action='store_true',
                        help="Launch simple gnuplot script in X11-terminal to check out results.")
    parser.add_argument('-o', '--outFile',
                    type=argparse.FileType('w'),
                    nargs='?',
                    default=sys.stdout,
                    help='File where to write oputput.')
    args = parser.parse_args()
    #pts_for_ne( [ pow(10, x[0]) for x in args.nIncedentElectrons], args.outFile )
    extrapolated_pts_for_ne( [ x[0] for x in args.nIncedentElectrons], args.outFile )
    #if args.plot:
    #    plotStrPrefixUnfmtd = "'%s' u 1:%d"
    #    for Ne in 


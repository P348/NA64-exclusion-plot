import sys, math
from preprocessPoints import sort_set_clockwise
from scipy import interpolate as interMod
import numpy as np

if 3 != len(sys.argv) or \
sys.argv[2] not in ('source', 'interpolation'):
    sys.stderr.write( 'Usage:\n    $ %s <datFile> (source|interpolation)\n'%sys.argv[1] )
    exit(1)
mode = sys.argv[2]

def numbers_gen( fname ):
    with open( fname ) as f:
        for l in f:
            ns = tuple( float(x) for x in f.readline().strip().split() )
            if not ns: continue
            yield ns
exclPoints = [ (ns[0], ns[1]) for ns in numbers_gen(sys.argv[1]) if ns[2] > 0 ]

# Input data is a grid of the g_{agg}-vs-m_a, so to infer the enclosing
# external contour we have to find the maximal and minimal values for some
# choosen column.
# - arrange the data over the m_a:
pts = {}
for p in exclPoints:
    if p[0] not in pts:
        pts[p[0]] = [ math.nan, math.nan ]
    if math.isnan(pts[p[0]][0]) or pts[p[0]][0] > p[1]:
        pts[p[0]][0] = p[1]
    if math.isnan(pts[p[0]][1]) or pts[p[0]][1] < p[1]:
        pts[p[0]][1] = p[1]

hullData = []
for ma in sorted(pts.keys()):
    hullData.append( [10**ma, 10**pts[ma][0]] )
    hullData.append( [10**ma, 10**pts[ma][1]] )

sortedPoints, hullParameters = sort_set_clockwise( hullData )

# Print points not being sorted
sortedPoints_ = []
for p in sortedPoints:
    if 'source' == mode:
        if len(p) == 4:
            print( "%e\t%e\t%e\t%e"%(p[0], p[1], p[2], p[3]))
        elif len(p) == 2:
            print( "%e\t%e"%(p[0], p[1]))
    if len(sortedPoints_):
        if .0 == sortedPoints_[-1][3] - p[3]:
            continue
    sortedPoints_.append(p)

if 'source' == mode: exit(0)

# Interpolate in radial frame
interpolatedF = interMod.UnivariateSpline(
            list(p[3] for p in sortedPoints_),
            list(p[2] for p in sortedPoints_),
            s=.2)

#print('# Interpolated data')
frng = (sortedPoints[0][3], sortedPoints[-1][3])
nPoints = 210
interpolated = list(map(lambda arg: [ arg, float(interpolatedF(arg)) ],
                        list([ x for x in np.arange( frng[0], frng[1],
                                                    (frng[1] - frng[0])/nPoints )
                            ])
                        ))

for p in interpolated:
    rho = p[1]
    theta = p[0]
    x = math.exp(rho*math.cos(theta) + hullParameters[0])
    y = math.exp(rho*math.sin(theta) + hullParameters[1])
    print( ( "%e\t%e\t%e\t%e"%( rho, theta, x, y ) ) )

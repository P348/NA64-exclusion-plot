#!/usr/bin/env python3

"""
Quick'n'dirty solution to combine the major invisible mode A' decay curve in
range 1e-3 - 1e1 GeV with extension obtained by D. Kirpichnikov at Oct/2017.

This is a non-export script, barely a calculation notepad that shall be ran
standalone.
"""

#from plot3SigmaRegions import pts_for_ne, tableData
import plot3SigmaRegions
from scipy import interpolate as interMod
from numpy import polyfit
import sys, os, math

import matplotlib.pyplot as plt  # for interim control

if __name__ == "__main__":
    if len(sys.argv) != 2:
        sys.exit('The single cmd-line argument shall denote the output file.')

def parse_two_column_file( path ):
    ret = []
    with open( path, 'r' ) as f:
        for line in f:
            data = list(map( lambda s : float(s), filter( lambda s : len(s.strip()), line.split(' '))))
            if not len(data):
                continue  # skip empty lines
            assert( 2 == len(data) )
            ret.append( [data[0], data[1]] )
    return ret

def rescale_to_scalar( factor, dPhotEps ):
    return math.sqrt(factor)*dPhotEps

def pgb_fA( rescaledEpsA4e12 ):
    """
    SNG claims Dr. Krasnikov obtained this equation. Gave me the sheet with
    formulas without detailed explaination. Corresponds to plot at the 12 slide
    of Pseudo-Goldstone discussion (see graphicSrc/pseudo-goldstone-boson.png
    source). The rescaledEpsA4e12 must correspond to 4e12 EOT.
    """
    return rescaledEpsA4e12*0.3/0.105

baseDir = os.path.dirname(os.path.abspath(__file__))

factorFileName = "FactorSquared_For_Pseudoscalar_vs_M.txt"
extnddFileName = "Temporary_epsilon_Vs_MA_NA64_For_MA_lighter_1MeV.txt"

factors = parse_two_column_file( os.path.join( baseDir, 'nData', factorFileName ) )

extended = parse_two_column_file( os.path.join( baseDir, 'nData', extnddFileName ) )

baseEpsilon = 2.7e10  # todo: or 4.3e10?

#original = map( lambda e : [e[0], e[1]], tableData[1:] )
original = plot3SigmaRegions.pts_for_ne( [baseEpsilon,], td=plot3SigmaRegions.tableData[1:] )

combined = extended + original

# Dump combined dataset:
#for e in combined:
#    print( "{0} {1}".format(*e) )

#combined4e10EOT = plot3SigmaRegions.pts_for_ne( [(2.7/4.3)*4e10,]
#                                              , td=combined )
# ^^^ Recalculated curve for N_{EOT} = 4e12
# XXX: wrong, the pts_for_ne() uses the base epsilon=1e-4

# Interpolate combined (full) table of A' mass vs. mixed parameter:
epsInterpF = interMod.InterpolatedUnivariateSpline(
          list(map(lambda x : x[0], combined))
        , list(map(lambda x : x[1], combined))
        , k=3
    )
# Interpolate factors function:
factorsInterpF = interMod.InterpolatedUnivariateSpline(
          list(map(lambda x : x[0], factors))
        , list(map(lambda x : x[1], factors))
    )

# Dump result using combined points from both tables:
interestingPoints = set(dict(combined).keys()).union(set(dict(factors).keys()))
rescaled = []
f = open(sys.argv[1], 'w')
f.write( "# m_{A'}  eps_{vect}  factor      eps_{scalar}  1/f_{A'} at EOT=4e10\n" )
for mA in sorted(list(interestingPoints)) + [6,]:
    scalarEps = rescale_to_scalar(factorsInterpF(mA), epsInterpF(mA))
    rescaledP = [ mA
                , scalarEps
                , pgb_fA(scalarEps) ]
    rescaled.append(rescaledP)
    # Dr. Krasnikov reported (green curve):
    # https://indico.cern.ch/event/672732/contributions/2752430/attachments/1540080/2414863/Epsilon_Vs_MA_NA64paper_fig14.pdf
    s = '{mA:.2e}  {epsAPrime:.4e}  {factor:.4e}  {rescaled:e}  {f_A:e}\n' \
            .format( mA=mA
                   , epsAPrime=float(epsInterpF(mA))
                   , factor=float(factorsInterpF(mA))
                   , rescaled=rescaledP[1]
                   , f_A=rescaledP[2]
                   )
    if f:
        f.write( s )

#plt.plot( map(lambda e : e[0], combined)
#        , map(lambda e : e[1], combined)
#        , 'o'
#        , map(lambda e : e[0], rescaled)
#        , map(lambda e : e[1], rescaled)
#        , '-')
#plt.xscale('log')
#plt.yscale('log')
#plt.show()



#!/bin/env python3

# Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
# Author: Renat R. Dusaev <crank@qcrypt.org>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

"""
The script provides some pre-processing operations for manually added points
(see http://arohatgi.info/WebPlotDigitizer/app/ for JS-tool): finds an
average point for each dataset, reorder data set to obtain a hull,
intepolate it in polar coordinates and write out.

By default, it will plot resulting data using gnuplot util.

For available options, see --help option.
"""

import json, os
from subprocess import Popen, PIPE, STDOUT
from math import sqrt, atan2, log, exp, pi, cos, sin
from operator import itemgetter
from scipy import interpolate as interMod
from numpy import arange
from sys import argv
import argparse
#from string import join as join_strings

# Template strings
commonPlotHdr = """# This is an automatically generated script.
# Manual changes aren't recommended.
"""
stagePlottingCommands = {
    'source'                : {
        'commonHeader' : "set log xy\nplot ",
        'dsPlotLine'   : "'%(wd)s/%(SetName)s.dat' u 1:2 w linespoints title \"%(SetName)s points\"",
    },
    'polar'                 : {
        'commonHeader' : "set polar\nplot ",
        'dsPlotLine'   : "'%(wd)s/%(SetName)s.dat' u 4:3 w linespoints title \"%(SetName)s points\"",
    },
    'interpolated-linear'   : {
        'commonHeader' : "plot ",
        'dsPlotLine'   : "'%(wd)s/%(SetName)s_interp.dat' u 2:1 w lines title \"%(SetName)s interp\", '%(wd)s/%(SetName)s.dat' u 4:3 w linespoints title \"%(SetName)s points\"",
    },
    'interpolated-polar'    :  {
        'commonHeader' : "set polar\nplot ",
        'dsPlotLine'   : "'%(wd)s/%(SetName)s_interp.dat' u 2:1 w lines title \"%(SetName)s interp\", '%(wd)s/%(SetName)s.dat' u 4:3 w linespoints title \"%(SetName)s points\"",
    },
    'final'                 :  {
        'commonHeader' : "set log xy\nset style fill  transparent solid 0.50 noborder\nplot ",
        'dsPlotLine'   : "'%(wd)s/%(SetName)s_interp.dat' u 3:4 w filledcurves closed title \"%(SetName)s\""
    }
}

def sort_set_clockwise( srcSet, centerX=None,
                                centerY=None,
                                cFactorX=1.,
                                cFactorY=1.,
                                smoothFactor=None,
                                interpolationMethod=None,
                                **kwargs ):
    pSet = srcSet[:]  # copy
    # find center point:
    if not centerX:
        if centerY:
            raise Exception('X coordinate for center is not specified while Y was.')
        centerX = log(pSet[0][0])
        centerY = log(pSet[0][1])
        n = 0
        for p in pSet[1:]:
            centerX += log(p[0])
            centerY += log(p[1])
            n += 1
        centerX /= cFactorX*n  # just shift...
        centerY /= cFactorY*n
    else:
        centerX = log(centerX)
        centerY = log(centerY)
    #print( "center: %e %e"%(exp(centerX), exp(centerY)) )
    for p in pSet:
        x = log(p[0])
        y = log(p[1])
        dx = x - centerX
        dy = y - centerY
        pRho = sqrt( dx**2 + dy**2 )
        pTheta = atan2(dy, dx)
        #if pTheta < 0:
        #    pTheta = 2*pi + pTheta
        #print( "rho = %e, theta = %e"%(pRho, pTheta) )
        p.append(pRho)
        p.append(pTheta)
    return [sorted( pSet, key=itemgetter(3) ), [centerX, centerY]]

if "__main__" == __name__:
    #
    # Argument parser.
    parser = argparse.ArgumentParser(description="Exclusion plot for A' script.")
    parser.add_argument('datasets', metavar='dataset', action='append', nargs='*',
                    help='Data set to be processed. If unset, then all found datasets will be processed.')
    parser.add_argument('--stage',
                    choices=['source', 'polar', 'interpolated-linear', 'interpolated-polar', 'final'],
                    default='final',
                    help='Specifies which on which stage data is to be plotted.')
    parser.add_argument('--list-datasets', action='store_true',
                    help='Just prints out the data set names and quits.')
    parser.add_argument('--sort',
                    choices=['polar', 'none', 'vicinity'],
                    default='polar',
                    help='Specifies which on which stage data is to be plotted.')
    parser.add_argument('--wd',
                    default='/tmp',
                    help='Working directory.')
    parser.add_argument('-i', '--jsonSource',  # TODO: make it list!
                    #default="../../p348g4/doc/exclusionPlot/nData/wpd_plot_data.json",
                    type=argparse.FileType('r'),
                    help='Just prints out the data set names and quits.',
                    action='append')
    parser.add_argument('--dumpGPScript', action='store_true',
                    help='Prints gnuplot script to stdout.')
    parser.add_argument('--noplot', action='store_true',
                    help='Do not launch a gnuplot subprocess after processing.')
    parser.add_argument('-c', '--manual-spec',
                    type=argparse.FileType('r'),
                    help='Manual parameters for named data sets specification file.')
    parser.add_argument('--dumpNiceJS', action='store_true',
                    help='"Pretty-prints" input JSON file and terminates the script.')
    args = parser.parse_args()

    #
    # Do the things.

    # Read manual specificated instructions, if provided.
    manualParameters = {}
    if args.manual_spec:
        exec( args.manual_spec.read() )

    # Read out JSON numericals (http://arohatgi.info/WebPlotDigitizer/app/ format)
    jdataStr = ""
    for jsonSource in args.jsonSource:
        with jsonSource as jdatFile:
            jdataStr=jdatFile.read()
        jDat = json.loads(jdataStr)
        #ver = jDat_['version'] if 'version' in jDat_.keys() else jDat_['wpd']['version']
        #jDat = {}
        #print( "Data file version: %d.%d."%(ver[0], ver[1]) )
        #if ver[0] < 4:
        #    jDat = jDat_['wpd']['dataSeries']
        #elif ver[0] >= 4:
        #    jDat = jDat_['datasetColl']
        #else:
        #    raise Exception("Unknown .json data file version format: %s"%str(ver))

        if 'wpd' in jDat:
            dsColl = jDat['wpd']['dataSeries']
        elif 'datasetColl' in jDat:
            dsColl = jDat['datasetColl']
        else:
            raise BadValue('Data format is not supported.')


        # Get API version. We're currently supporting only 3 and 4 major
        # versions.
        #try:
        #    aVer = jDat['wpd']['version']
        #except KeyError:
        #    aVer = jDat['version']
        #if aVer[0] < 4:
        #    jDat = jDat['wpd']  # trivial key
        #if args.listDatasets:
        #    #print( 'Available datasets in %s'%args.jsonSource )
        #    for dataSet in jDat['dataSeries' if aVer[0] < 4 else 'datasetColl']:

        if args.list_datasets:
            #print( 'Available datasets in %s'%args.jsonSource )
            for dataSet in dsColl:
                print(dataSet['name'])
            exit(0)

        if args.dumpNiceJS:
            print( json.dumps(jDat, indent=4, sort_keys=True) )
            exit(0)

        # Process each choosen (or all found) sets
        plotStrTuple = []
        for dataSet in dsColl:
            if len(args.datasets[0]):
                if not dataSet['name'] in args.datasets[0]:
                    continue
                else:
                    print( dataSet['name'] )
            # prepare formatting dictionary
            fmtDct = {
                'wd' : args.wd,
                'SetName' : dataSet['name']
            }
            #print( 'XXX', dataSet['name'] )
            # extract points from JSON
            if dataSet['name'] in manualParameters and \
                'noradial' in manualParameters[dataSet['name']] \
                and manualParameters[dataSet['name']]['noradial']:
                noRadial = True
            else:
                noRadial = False
            sortedPoints = []
            hullParameters = None
            extracted2sort = list(map( lambda s: [s['value'][0], s['value'][1]], dataSet['data'] ))
            if not len(extracted2sort):
                continue
            # sort in polar coordinates
            if args.sort == 'polar':
                if dataSet['name'] in manualParameters.keys():
                    sortedPoints, hullParameters = sort_set_clockwise( extracted2sort, **(manualParameters[dataSet['name']]) )
                else:
                    sortedPoints, hullParameters = sort_set_clockwise( extracted2sort )
            elif args.sort == 'vicinity':
                raise NotImplementedError( 'Vicinity sorting is not implemented yet.' )  # TODO
            else:  # none
                sortedPoints = extracted2sort
            # write sorted data [x, y, rho, theta]
            fp = os.path.join( r'%(wd)s'%fmtDct, r'%(SetName)s.dat'%fmtDct )
            outFile = open( fp, 'w' )
            for p in sortedPoints:
                if len(p) == 4:
                    outFile.write( "%e\t%e\t%e\t%e\n"%(p[0], p[1], p[2], p[3]))
                elif len(p) == 2:
                    outFile.write( "%e\t%e\n"%(p[0], p[1]))
            outFile.close()
            if noRadial:
                xGetter = lambda p: p[0]
                yGetter = lambda p: p[1]
            else:
                xGetter = lambda p: p[3]
                yGetter = lambda p: p[2]
            if args.stage != 'source' and args.stage != 'polar':
                # interpolate
                interpolatedF = None
                if dataSet['name'] in manualParameters and \
                   'interpolationMethod' in manualParameters[dataSet['name']]:
                    if manualParameters[dataSet['name']]['interpolationMethod'] == 'UnivariateSpline':
                        smoothFactor = 0.9
                        if dataSet['name'] in manualParameters and \
                            'smoothFactor' in manualParameters[dataSet['name']]:
                            smoothFactor = manualParameters[dataSet['name']]['smoothFactor']
                        interpolatedF = interMod.UnivariateSpline(
                            list(map(xGetter, sortedPoints)),
                            list(map(yGetter, sortedPoints)),
                            s=smoothFactor)
                    elif manualParameters[dataSet['name']]['interpolationMethod'] is None:
                        continue
                else:
                    try:
                        interpolatedF = interMod.interp1d(
                            list(map(xGetter, sortedPoints)),
                            list(map(yGetter, sortedPoints)),
                            kind='cubic')
                    except Exception as e:
                        print( "In data set \"%s\""%dataSet['name'] )
                        raise e
                rng = None
                if dataSet['name'] in manualParameters and \
                        'range' in manualParameters[dataSet['name']]:
                    rng = eval( manualParameters[dataSet['name']]['range'] )
                else:
                    # default --- use the original range (no extrapolation)
                    rng = arange( sortedPoints[0][3],
                                  sortedPoints[-1][3],
                                  (sortedPoints[-1][3] - sortedPoints[0][3])/210 )
                try:
                    if dataSet['name'] in manualParameters.keys() \
                    and 'overrideRange' in manualParameters[dataSet['name']].keys():
                        c = manualParameters[dataSet['name']]['overrideRange']
                        # Recalculate the range w.r.t. to given polar->linear
                        # mappings:
                        angularLength = (sortedPoints[-1][3] - sortedPoints[0][3])
                        frng = ( c['range'][0]*angularLength + sortedPoints[0][3]
                               , c['range'][1]*angularLength + sortedPoints[0][3] )
                        nPoints = c.get('nPoints', 210)  # TODO: configurable?
                    else:
                        frng = (sortedPoints[0][3], sortedPoints[-1][3])
                        nPoints = 210  # TODO: configurable?
                    interpolated = list(map(
                        lambda arg: [ arg, float(interpolatedF(arg)) ],
                        list([ x for x in arange( frng[0], frng[1],
                                                  (frng[1] - frng[0])/nPoints )
                            ])
                        ))
                except ValueError as e:
                    print( "In data set \"%s\" on range [%e:%e]"%(dataSet['name'], numArgsLst[0], numArgsLst[-1]) )
                    raise e
                # write interpolated data
                fp = os.path.join( r'%(wd)s'%fmtDct, r'%(SetName)s_interp.dat'%fmtDct )
                outFInt = open( fp, 'w' )
                for p in interpolated:
                    try:
                        rho = p[1] if not noRadial else float('NaN')
                        theta = p[0] if not noRadial else float('NaN')
                        x = exp(rho*cos(theta) + hullParameters[0]) if not noRadial else p[0]
                        y = exp(rho*sin(theta) + hullParameters[1]) if not noRadial else p[1]
                    except OverflowError as e:
                        print( "In data set \"%s\""%dataSet['name'] )
                        print( "Unable to represent interpolated data (overflow):" )
                        print( "    rho=%e, theta=%e, hullParameters={%e, %e}"%(
                            rho, theta, hullParameters[0], hullParameters[1] ) )
                        x = y = float('nan')
                    outFInt.write( "%e\t%e\t%e\t%e\n"%( rho, theta, x, y ) )
                outFInt.close()
            # append plotting string with one
            plotStrTuple.append( stagePlottingCommands[args.stage]['dsPlotLine']%fmtDct )
        # now plot all the stuff
        plotStr = commonPlotHdr + '\n' + stagePlottingCommands[ args.stage ]['commonHeader'] + \
                                  ', \\\n'.join( plotStrTuple ) + '\n'
        if args.dumpGPScript:
            print( plotStr )
        if not args.noplot:
            p = Popen(['gnuplot', '-persistant'], stdout=PIPE, stdin=PIPE, stderr=STDOUT)
            gpOut = p.communicate(input=plotStr.encode('utf-8'))[0]
            if gpOut:
                print(gpOut.decode('utf-8'))


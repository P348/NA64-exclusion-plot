
from math import log10, log

"""
This file contains some auxilliary definitions for numerical procedures.
"""

#
# The linear BaBar curve section taken from DOE conference is apparently a bit
# inaccurate. For the sake of representativeness, here we define additional
# piecewise function correcting the plot:
#
#                            / f1(m_chi)
#   f(m_chi) = m_chi > y2 ? <
#                            \ f2(m_chi)
#
# where f1(m_chi) obeys following conditions:
#
# f1(x1) = y1
# f1(x0) = 1   => 10^(k*x) = 10^(-m) => k*x = -m
# f1(x2) = y2
#
# Also we demand that f1 is being linear function in logarithmic scale, so:
#
#       f1(x) = 10^(k*x + m)
#
# which gives:
#
#       k = log10(y1/y2) / (x1 - x3)
#       m = x2 * m

def k(x1, x2, x3, y1, y2):
    return log10(y1 / y2) / (x1-x3)

def m(x1, x2, x3, y1, y2):
    return - x2 * k(x1, x2, x3, y1, y2)

def f1( x, k, m ):
    return 10**( k*x + m )

# - For ``Majorana Thermal DM'' plot we demand:
# x1 = 1e-3, y1 = 1.05
# x2 = 1.5e-2
# x3 = 8e-1, y2 = 0.95

mjX1 = 3e-3
mjX2 = 1.5e-2
mjX3 = mjX2*mjX2/mjX1

mjY1 = 1.05
mjY2 = 1/mjY1

mjArgs = [ mjX1, mjX2, mjX3, mjY1, mjY2 ]

mjM = m(*mjArgs)
mjK = k(*mjArgs)

print( 'Majorana: m=%e, k = %e'%( mjM, mjK ) )
print( '  f1(x1=%e) = %e (vs %e)'%( mjX1, f1(mjX1, mjK, mjM), mjY1 ) )
print( '  f1(x2=%e) = %e (vs %e)'%( mjX2, f1(mjX2, mjK, mjM), 1    ) )
print( '  f1(x3=%e) = %e (vs %e)'%( mjX3, f1(mjX3, mjK, mjM), mjY2 ) )


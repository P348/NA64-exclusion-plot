#!/bin/env gnuplot

# Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
# Author: Renat R. Dusaev <crank@qcrypt.org>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

set terminal context size 15cm,11.5cm standalone fontscale 0.8
set out outFile

set colorsequence podo
set termoption dashed

set xlabel "$m_{\\chi}, MeV$"
set ylabel "$y = \\epsilon^2 \\alpha_{D} (m_{\\chi}/m_{A'})^4$"
set xrange [8:1.4e3]
set yrange [1e-12:2e-7]

set samples 5000

# This function is for a_mu area. Fitted approximately (see filledRegionTest.gpi)
a_mu_intern(x) = 3.47e-9*x + 1.46e-8
a_mu_extern(x) = 1.53e-9*x + 6.3e-10
a_mu_fill_stub(x) = ( x > 53.6 && x < 134) ? a_mu_extern(x) : 1./0

set log xy
#set grid x y

set style fill transparent solid 0.1
set format x '$10^{%L}$'
set xtics add ("1"  1)
set xtics add ("10" 10)
set format y '$10^{%L}$'

#set title "Thermal Relic Targets and Current Constants"

set style line  1 lc rgb "#00abbd"        # LSND
set style line  2 lc rgb "#926312"        # E137
set style line  3 lc rgb "#4478da" lw .5  # NA64
set style line  4 lc rgb "#44782b" lw .5  # MiniBooNE
set style line  5 lc rgb "#aa782b" lw .5  # BaBar
set style line  6 lc rgb "#77aa00" lw .5  # Belle II / XENON10
set style line 10 lc rgb "#2a9966"        # Direct detection nucleon
set style line 11 lc rgb "#000000" lw 1.5 # Targets (Relics: Majorana/El.-inel., Pseudo-Dirac)
set style line 14 lc rgb "#001eff" lw .5  # K->pi, inv
set style line 22 lc rgb "#333333" lw .5  # A_mu favored

#     datDir.'/tldra-19-2.dat-dir/E137_interp.dat' u 3:4 w lines notitle ls 2, \

set label 3  'BaBar' at 1.8e2,8e-9 center tc ls 5
set label 4  "XENON10" at 12,6.4e-10 center tc ls 6
set label 5  "Direct\nDetection\nNucleon" at 6.9e2,6e-8 center tc ls 10
set label 6  "LSND" at 1e1,2e-11 center tc ls 1
set label 7  "E137" at 1e1,1.5e-10 center tc ls 2
set label 8  "NA64" at 5.4e1,2.5e-9 center tc ls 3
set label 9  "MiniBooNE" at 1.2e1,3.4e-9 center tc ls 4
set label 10 '$a_{\mu}$ favored' at 12,2e-8 center tc ls 22

set label 11 'Pseudo-Dirac Fermion Relic' at 3.6e1,5e-12 rotate by 38 center tc ls 11 front
set label 12 'Majorana Relic' at 2.45e1,1.45e-11 rotate by 36 center tc ls 11 front
set label 13 'Elastic & Inelastic Scalar Relic' at 3.1e1,9e-11 rotate by 36 center tc ls 11 front

set label 24 "$K^{+}$ \\rightarrow \n $\\pi^{+}$ + invis." at 2.8e1,4.2e-8 center tc ls 14 front

# NOTE: to check, LSND_interp.dat => LSND-19_interp.dat
#                 tldra-19-2.dat-dir/BaBar_interp.dat => tldra-19-2-alpha_0.5.dat-dir/BaBar_interp.dat

plot datDir.'/tldra-19-2-cutted-tails.dat-dir/Dirac-Fermion-Relic_interp.dat' u 3:4 smooth csplines w lines notitle ls 11, \
     datDir.'/tldra-19-2-cutted-tails.dat-dir/El-Inel-Scalar-Relic_interp.dat' u 3:4 smooth csplines w lines notitle ls 11, \
     datDir.'/tldra-19-2-cutted-tails.dat-dir/Majorana-Relic-Target_interp.dat' u 3:4 smooth csplines w lines notitle ls 11, \
     datDir.'/tldra-19-2-alpha_0.5.dat-dir/BaBar_interp.dat' u 3:4 w filledcu x2 notitle ls 5, \
     \
     datDir.'/E137-fermion.dat' u ($1*1e3/3.):($2*$2*.19/81.) smooth bezier w lines notitle ls 2, \
     \
     \
     datDir.'/LSND-19_interp.dat' u ($3*1e3/3.):($4*$4*.25/81.) smooth bezier w lines notitle ls 1, \
     \
     datDir.'/mnbe/XENON10_interp.dat' u 3:4 w filledcu x2 notitle ls 6, \
     datDir.'/mnbe/K_pi_invis_1_interp.dat' u 3:4 w lines notitle ls 14, \
     datDir.'/mnbe/K_pi_invis_2_interp.dat' u 3:4 w lines notitle ls 14, \
     datDir.'/mnbe/MiniBooNE_interp.dat' u 3:4 w filledcu x2 notitle ls 4, \
     \
     datDir.'/mnbe/A_mu_favored_1_interp.dat' u 3:4:(a_mu_extern($3)) w filledcu closed ls 22 notitle, \
     datDir.'/mnbe/A_mu_favored_2_interp.dat' u 3:(a_mu_extern($3)):4 w filledcu closed ls 22 notitle, \
     a_mu_fill_stub(x) w filledcu x2 ls 22 notitle, \
     \
     datDir.'/mnbe/Direct Detection Nucleon_interp.dat' u 3:4 w lines notitle ls 10, \
     \
     scriptDir.'/nData/mk-limits-17.08.017.dat' u 4:($2*$2*1.25*$5*1e-8)*0.61/162 smooth bezier w filledcu x2 notitle ls 3



# Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
# Author: Renat R. Dusaev <crank@qcrypt.org>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

manualParameters = {
    "MiniBooNE" : {
        'centerX' : 2.5e2,
        'centerY' : 1e-1,
        'interpolationMethod' : 'UnivariateSpline',
        'smoothFactor' : .1,
    },
    "XENON10" : {
        'centerX' : 12,
        'centerY' : 1e-7,
        'interpolationMethod' : 'UnivariateSpline',
        'smoothFactor' : .01,
    },
    "K_pi_invis_1" : {
        'centerX' : 30,
        'centerY' : 1e-5,
        'interpolationMethod' : 'UnivariateSpline',
        'smoothFactor' : .1,
    },
    "K_pi_invis_2" : {
        'centerX' : 60,
        'centerY' : 1e-4,
        'interpolationMethod' : 'UnivariateSpline',
        'smoothFactor' : .1,
    },
    "A_mu_favored_1" : {
        'centerX' : 8,
        'centerY' : 1e-6,
        'interpolationMethod' : 'UnivariateSpline',
        'smoothFactor' : .1,
    },
    "A_mu_favored_2" : {
        'centerX' : 8,
        'centerY' : 1e-6,
        'interpolationMethod' : 'UnivariateSpline',
        'smoothFactor' : .1,
    },
    "Relic Density" : {
        'interpolationMethod' : 'UnivariateSpline',
        'smoothFactor' : .01,
        'centerX' : 10,
        'centerY' : 1e-7,
    },
    "Direct Detection Nucleon" : {
        'interpolationMethod' : 'UnivariateSpline',
        'smoothFactor' : .01,
        'centerX' : 2e3,
        'centerY' : 1e-3,
    },
    #
    "E137_dmmnb" : {
        'centerX' : 5e-2,
        'centerY' : 1e-9,
    },
    "E137_dmmnb2" : {
        'centerX' : 5e-3,
        'centerY' : 1e-9,
    },
    #
    'MB_el_pl_timing' : {
        'centerX' : 2.5e-1,
        'centerY' : 1e+3,
        'interpolationMethod' : 'UnivariateSpline',
        'smoothFactor' : .1,
    },
    'mb_el_pl_timing_dashed' : {
        'centerX' : 2.5e-1,
        'centerY' : 1e-1,
        'interpolationMethod' : 'UnivariateSpline',
        'smoothFactor' : .1,
    },
    'mb_full_n_pl_timing' : {
        'centerX' : 2.5e-1,
        'centerY' : 1e-14,
        'interpolationMethod' : 'UnivariateSpline',
        'smoothFactor' : .1,
    },
    'mb_full_n_pl_timing_dashed' : {
        'centerX' : 2.5e-1,
        'centerY' : 1e-1,
        'interpolationMethod' : 'UnivariateSpline',
        'smoothFactor' : .1,
    },
    'relic_density' : {
        'centerX' : 2.5e-1,
        'centerY' : 1e-1,
        'interpolationMethod' : 'UnivariateSpline',
        'smoothFactor' : .1,
    },
    'mb_elastic_n' : {
        'centerX' : 2.5e-1,
        'centerY' : 1e-1,
        'interpolationMethod' : 'UnivariateSpline',
        'smoothFactor' : .1,
    }
}


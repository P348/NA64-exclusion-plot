manualParameters = {
    "NuCal-2020" : {
        'centerX' : 2e-3,
        'centerY' : 1e-3,
        'interpolationMethod' : 'UnivariateSpline',
        'smoothFactor' : .5,
    },
    "CHARM-2020" : {
        'centerX' : 1e-2,
        'centerY' : 1e-4,
        'interpolationMethod' : 'UnivariateSpline',
        'smoothFactor' : .5,
    },
}


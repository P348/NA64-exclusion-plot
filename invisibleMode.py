# Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
# Author: Renat R. Dusaev <crank@qcrypt.org>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

manualParameters = {
    'BaBar' : {
        'centerX' : 5,
        'centerY' : .5,
        'interpolationMethod' : 'UnivariateSpline',
        'smoothFactor' : .4
    },
    'E787' : {
        'centerX' : .05,
        'centerY' : .6
    },
    'DarkLight' : {
        'centerX' : .05,
        'centerY' : .1,
        'interpolationMethod' : 'UnivariateSpline',
        'smoothFactor' : .03
    },
    'BaBar improved' : {
        'centerX' : 5,
        'centerY' : .5,
        'interpolationMethod' : 'UnivariateSpline',
        'smoothFactor' : .1
    },
    'BaBar-2017' : {
        'centerX' : 5.7,
        'centerY' : 10,
        'interpolationMethod' : 'UnivariateSpline',
        'smoothFactor' : .1
    },
    'E949' : {
        'centerX' : .2,
        'centerY' : .02,
        'interpolationMethod' : 'UnivariateSpline',
        'smoothFactor' : .01
    },
    'VEPP-3' : {
        'centerX' : 0.015,
        'centerY' : 0.011,
        'interpolationMethod' : 'UnivariateSpline',
        'smoothFactor' : .01
    },
    'Belle II standard' : {
        'centerX' : 4,
        'centerY' : .05,
    },
    'Belle II converted (a, b)' : {
        'centerX' : 4,
        'centerY' : .005,
    },
    'Belle low-E' : {
        'centerX' : 4,
        'centerY' : .005,
    },
    'LSND' : {
        'centerX' : .01,
        'centerY' : .005,
    },
    'ORKA1' : {
        'centerX' : .02,
        'centerY' : 5e-2,
        'interpolationMethod' : 'UnivariateSpline',
        'smoothFactor' : .1
    },
    'ORKA2' : {
        'centerX' : .2,
        'centerY' : 1e-1,
        'interpolationMethod' : 'UnivariateSpline',
        'smoothFactor' : .1
    },
    'a_mu favored outern' : {
        'centerX' : 1e-3,
        'centerY' : 1e-2,
    },
    'a_mu favored intern' : {
        'centerX' : 1e-3,
        'centerY' : 1e-2,
    },
    'a_mu' : {
        'centerX' : 1e-3,
        'centerY' : 1e-2,
    },
    'a_e' : {
        'centerX' : 1e-3,
        'centerY' : 1e-2,
    },
    'K_mode' : {
        'centerX' : 1e-3,
        'centerY' : 1e-2,
    },
    '10up9_100GeV' : {
        'centerX' : 1e-3,
        'centerY' : 1e-2,
    },
    '10up9_30GeV' : {
        'centerX' : 1e-3,
        'centerY' : 1e-2,
    },
    '10up12_30GeV' : {
        'centerX' : 1e-3,
        'centerY' : 1e-2,
    },
    '10up12_100GeV' : {
        'centerX' : 1e-3,
        'centerY' : 1e-2,
    },
    'NA62' : {
        'centerX' : 8e-2,
        'centerY' : 1e-4,
    },
    'g-2-high' : {
        'centerX' : 1e-3,
        'centerY' : 1e-2,
        'interpolationMethod' : 'UnivariateSpline',
        'smoothFactor' : .4,
        'range' : "[x*10**y for y in range(-3, 1) for x in range(1, 11)]",
        'noradial' : True,
    },
    'g-2-low' : {
        'centerX' : 1e-3,
        'centerY' : 1e-2,
        'interpolationMethod' : 'UnivariateSpline',
        'smoothFactor' : .4,
        'range' : "[x*10**y for y in range(-3, 1) for x in range(1, 11)]",
        'noradial' : True,
    },
    'Borexino' : {
        'centerX' : 1e-2,
        'centerY' : 1e-1,
        'interpolationMethod' : 'UnivariateSpline',
        'range' : "[x*10**y for y in range(-3, 1) for x in range(1, 11)]",
        'noradial' : True,
    },
    'CCFR' : {
        'centerX' : 1e-1,
        'centerY' : 1,
        'interpolationMethod' : 'UnivariateSpline',
        'range' : "[x*10**y for y in range(-3, 1) for x in range(1, 11)]",
        'noradial' : True,
    }
}

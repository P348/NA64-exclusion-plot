Directories:
 * alps.brick/ -- data for axion-like particles plot provided by Dmitry K.
 * pseudoscalars.brick/ -- data for pseudoscalars provided by Dmitry K.
Files in the directory and ther description:
 * 70_MeV_table1_II.txt
 * 70_MeV_table1.txt
 * alphamu.json
 * alps-019-manual.dat
 * alps-019-manual.json
 * alps-020-manual.dat
 * BaBar-017.json
 * borexino-Nov-2017.json
 * CHARMpts_.dat
 * couplings-muon-1.json
 * couplings-muon-2.json
 * _description.txt
 * dm-minib-fig25-a.json
 * dm-minib-fig25-b.json
 * dm-minib-fig26-a.json
 * E137.json
 * E141-Babette.dat
 * ebeam_80_table1.txt
 * FactorSquared_For_Pseudoscalar_vs_M.txt
 * Fig19.json
 * inh-tldra-20-1.json
 * inh-tldra-20-2.json
 * Millicharge.json
 * MiniBooNE-23-08-017.json
 * mk-limits-05.05.019.dat
 * mk-limits-06.10.017.dat
 * mk-limits-10.04.018.dat
 * mk-limits-17.08.017.dat
 * mk-limits-19.10.017.dat
 * mk-limits-vis-15-12-020-bezier.dat
 * mk-limits-vis-15-12-020.dat
 * na62-2019.json
 * new-stats-combined-june-2019.json
 * NuCAL-CHARM-2020-update.json
 * Nucalpts_.dat
 * physrevd-99-075001-2019-fig4-1.json
 * physrevd-99-075001-2019-fig4-2.json
 * physrevd-99-075001-2019-fig4-3.json
 * pseudo-goldstone-boson.json
 * results_MvsEps_VFF_alphaD0p1.dat -- alpha=.1 e+e- annih case (provided by Andrea Ch.)
 * results_MvsEps_VFF_alphaD0p5.dat -- alpha=.5 e+e- annih case (provided by Andrea Ch.)
 * sng-additional.dat
 * sng-visible-cropped.json
 * sng-visible-na64.json
 * table1_III.txt
 * Temporary_epsilon_Vs_MA_NA64_For_MA_lighter_1MeV.txt
 * tldra-19-1.json
 * tldra-19-2-alpha_0.1.json
 * tldra-19-2-alpha_0.5.json
 * tldra-19-2-cutted-tails.json
 * tldra-21-1.json
 * visMode-2019-q2.dat
 * wpd_plot_data.json
 * wpd_plot_data_k2_zoom.json
 * XENON1T-el.json

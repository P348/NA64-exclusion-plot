# Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
# Author: Renat R. Dusaev <crank@qcrypt.org>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

# dev note: see https://www.gnu.org/software/make/manual/html_node/Automatic-Variables.html
# for %-pattern expansion within the rule. I made a lot of mess with `*.anchor'
# here not knowing this shortcut.

          SCRIPT_DIR := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))
           BUILD_DIR := $(shell pwd)
INTERIM_DATFILES_DIR := $(BUILD_DIR)/interim_data
            EXEC_DIR := $(SCRIPT_DIR)/py

             GNUPLOT := gnuplot
               LATEX := context
               MKDIR := mkdir
 PREPROCESING_SCRIPT := $(EXEC_DIR)/preprocessPoints.py
  STATLINES_GENERATE := $(EXEC_DIR)/plot3SigmaRegions.py
                GREP := grep
                 AWK := awk

        STATLINES_NE := -n 1e9 -n 1e10 -n 1e11 -n 1e12 -n 1e13 -n 2.75e9 -n 4e10

#
# Visible mode plots
  DATASETS_VISIBLE_1 := $(SCRIPT_DIR)/nData/wpd_plot_data.json
  DATASETS_VISIBLE_2 := $(SCRIPT_DIR)/nData/wpd_plot_data_k2_zoom.json
  DATASETS_VISIBLE_3 := $(SCRIPT_DIR)/nData/sng-visible-cropped.json
     MANSPEC_VISIBLE := $(SCRIPT_DIR)/visibleMode.py
MINIBOONE_EXTRA_DATA := $(SCRIPT_DIR)/nData/MiniBooNE-23-08-017.json
  VISIBLE_DATASETS_1 := $(shell $(PREPROCESING_SCRIPT) -i $(DATASETS_VISIBLE_1) --list-datasets )
  VISIBLE_DATASETS_2 := $(shell $(PREPROCESING_SCRIPT) -i $(DATASETS_VISIBLE_2) --list-datasets )
  VISIBLE_DATASETS_3 := $(shell $(PREPROCESING_SCRIPT) -i $(DATASETS_VISIBLE_3) --list-datasets )
  VISIBLE_DATFILES_1 := $(patsubst %, $(INTERIM_DATFILES_DIR)/%.dat, $(VISIBLE_DATASETS_1))
  VISIBLE_DATFILES_2 := $(patsubst %, $(INTERIM_DATFILES_DIR)/%.dat, $(VISIBLE_DATASETS_2))
  VISIBLE_DATFILES_3 := $(patsubst %, $(INTERIM_DATFILES_DIR)/%.dat, $(VISIBLE_DATASETS_3))

#
# Invisible mode plots
    DATASETS_INVISIBLE := $(SCRIPT_DIR)/nData/Fig19.json $(SCRIPT_DIR)/nData/E137.json $(SCRIPT_DIR)/nData/BaBar-017.json $(SCRIPT_DIR)/nData/na62-2019.json $(SCRIPT_DIR)/nData/borexino-Nov-2017.json
     MANSPEC_INVISIBLE := $(SCRIPT_DIR)/invisibleMode.py
    INVISIBLE_DATASETS := $(shell $(PREPROCESING_SCRIPT) -i $(DATASETS_INVISIBLE) --list-datasets )
    INVISIBLE_DATFILES := $(patsubst %, $(INTERIM_DATFILES_DIR)/%.dat, $(VISIBLE_INDATASETS))

#
# TLDRA (Light thermal Dark Matter)
        TLDRA_DATFILES := $(wildcard $(SCRIPT_DIR)/nData/tldra-*.json)
         MANSPEC_TLDRA := $(patsubst $(SCRIPT_DIR)/nData/tldra-%.json,$(SCRIPT_DIR)/tldra-%.py,$(TLDRA_DATFILES))
       TLDRA_PLOTFILES := $(patsubst $(SCRIPT_DIR)/nData/tldra-%.json,$(SCRIPT_DIR)/gpis/tldra-%.gpi,$(TLDRA_DATFILES))
        TLDRA_DATASETS := $(foreach ds,$(TLDRA_DATFILES),$(shell $(PREPROCESING_SCRIPT) -i $(ds) --list-datasets))
             OUT_TLDRA := tldra

#
# MiniBooNE
 MINIBOONE_EXTRAS_DATA := $()
 MINIBOONE_DATASETS_NS := $(shell $(PREPROCESING_SCRIPT) -i $(MINIBOONE_EXTRA_DATA) --list-datasets )
    MINIBOONE_DATASETS := $(patsubst %, $(INTERIM_DATFILES_DIR)/mnbe-%.dat,$(MINIBOONE_DATASETS_NS))
     MANSPEC_MINIBOONE := $(SCRIPT_DIR)/MiniBooNE.py

#
# Pseudo-Goldstone Boson
              PGB_DATA := $(SCRIPT_DIR)/nData/pseudo-goldstone-boson.json
               OUT_PGB := pgb
       PGB_PLOT_SCRIPT := $(SCRIPT_DIR)/gpis/pseudo-goldstone.gpi
          DATASETS_PGB := $(shell $(PREPROCESING_SCRIPT) -i $(PGB_DATA) --list-datasets)
          PGB_DATFILES := $(patsubst %, $(INTERIM_DATFILES_DIR)/pgb/%.dat, $(DATASETS_PGB))
        PGB_EXTRA_DEPS := $(SCRIPT_DIR)/nData/Temporary_epsilon_Vs_MA_NA64_For_MA_lighter_1MeV.txt $(SCRIPT_DIR)/nData/FactorSquared_For_Pseudoscalar_vs_M.txt
     PGB_RECALC_SCRIPT := $(EXEC_DIR)/pgbCombine.py
PGB_NA64_EXCLUSION_FILE := $(INTERIM_DATFILES_DIR)/pgb/na64-combined.dat

#
# Alphamu
        ALPHAMU_JSDATA := $(SCRIPT_DIR)/nData/alphamu.json
       ALPHAMU_MANSPEC := $(SCRIPT_DIR)/alphamu.py
      ALPHAMU_DATASETS := $(shell $(PREPROCESING_SCRIPT) -i $(ALPHAMU_JSDATA) --list-datasets )

#
# XENON1T-el datasets
      XENON1T_DATASETS := xenon1t-optimistic xenon1t-pessimistic  # $(shell $(PREPROCESING_SCRIPT) -i $(SCRIPT_DIR)/nData/XENON1T-el.json --list-datasets )
      XENON1T_DATFILES := $(patsubst %, $(INTERIM_DATFILES_DIR)/xenon1t-cs/%.dat, $(XENON1T_DATASETS))

all: exclusionVisible.pdf \
     exclusionInvisible.pdf \
     tldra \
     mnbe.pdf \
	 borexino-combined.pdf \
     tables

tables: table.y-m_chi_alpha_D-0.1_EOT-4.3e10.dat \
        table.y-m_chi_alpha_D-0.1_EOT-5e11.dat \
        table.y-m_chi_alpha_D-0.1_EOT-5e12.dat \
        table.pd-tdm_EOT-5e10.dat \
		table.mj-tdm_EOT-5e10.dat \
		table.y-m_chi_alpha_D-0.1_EOT-2.84e11.dat

#
# Visible mode rules
exclusionVisible.pdf: exclusionVisible.tex
	@echo -e "\033[35;1mRendering plot $@\033[0m"
	$(LATEX) $^ > $@.latex-out.txt

exclusionVisible.tex: visibleModeDatfiles_1.anchor \
                      visibleModeDatfiles_2.anchor \
                      visibleModeDatfiles_3.anchor \
					  table.na64-vis.dat \
					  $(SCRIPT_DIR)/nData/visMode-2019-q2.dat \
                      $(SCRIPT_DIR)/gpis/na64-vis.gpi \
					  $(SCRIPT_DIR)/nData/mk-limits-vis-15-12-020.dat \
					  $(SCRIPT_DIR)/nData/mk-limits-vis-15-12-020-bezier.dat
	@echo -e "\033[36;1mPlotting \"Visible mode\"\033[0m"
	$(GNUPLOT) -e "datDir='$(INTERIM_DATFILES_DIR)';outFile='exclusionVisible';scriptDir='$(SCRIPT_DIR)'" $(SCRIPT_DIR)/gpis/na64-vis.gpi

visibleModeDatfiles_1.anchor: $(DATASETS_VISIBLE_1) $(MANSPEC_VISIBLE) | $(INTERIM_DATFILES_DIR)
	@echo -e "\033[32;1mPreprocessing \"visible datasets I\" $(DATASETS_VISIBLE_1)\033[0m"
	$(PREPROCESING_SCRIPT)  --wd $(INTERIM_DATFILES_DIR) \
			$(foreach src,$(DATASETS_VISIBLE_1),--jsonSource $(src)) --noplot --manual-spec $(MANSPEC_VISIBLE)
	@touch $@

visibleModeDatfiles_2.anchor: $(DATASETS_VISIBLE_2) $(MANSPEC_VISIBLE) | $(INTERIM_DATFILES_DIR)
	@echo -e "\033[32;1mPreprocessing \"visible datasets II\" $(DATASETS_VISIBLE_2)\033[0m"
	$(PREPROCESING_SCRIPT)  --wd $(INTERIM_DATFILES_DIR) \
			$(foreach src,$(DATASETS_VISIBLE_2),--jsonSource $(src)) --noplot --manual-spec $(MANSPEC_VISIBLE)
	@touch $@

visibleModeDatfiles_3.anchor: $(DATASETS_VISIBLE_3) $(SCRIPT_DIR)/nData/sng-visible-na64.json $(MANSPEC_VISIBLE) | $(INTERIM_DATFILES_DIR)
	mkdir -p $(INTERIM_DATFILES_DIR)/sng-v-cropped
	@echo -e "\033[32;1mPreprocessing \"visible datasets III\" $(DATASETS_VISIBLE_1)\033[0m"
	$(PREPROCESING_SCRIPT)  --wd $(INTERIM_DATFILES_DIR)/sng-v-cropped \
		--jsonSource $(SCRIPT_DIR)/nData/sng-visible-na64.json --noplot --manual-spec $(MANSPEC_VISIBLE)
	$(PREPROCESING_SCRIPT)  --wd $(INTERIM_DATFILES_DIR)/sng-v-cropped \
			$(foreach src,$(DATASETS_VISIBLE_3),--jsonSource $(src)) --noplot --manual-spec $(MANSPEC_VISIBLE)
	@touch $@

#
# Visible pseduoscalars

exclusionVisPS.pdf: exclusionVisPS.tex
	@echo -e "\033[35;1mRendering plot $@\033[0m"
	$(LATEX) $^ > $@.latex-out.txt

exclusionVisPS.tex: $(SCRIPT_DIR)/gpis/na64-vis-pseudoscalars.gpi
	@echo -e "\033[36;1mPlotting \"Visible pseudoscalars\"\033[0m"
	$(GNUPLOT) -e "datDir='$(INTERIM_DATFILES_DIR)';outFile='exclusionVisPS';scriptDir='$(SCRIPT_DIR)'" $<

#
# Invisible mode rules
exclusionInvisible.pdf: exclusionInvisible.tex
	@echo -e "\033[35;1mRendering plot $@\033[0m"
	$(LATEX) $^ > $@.latex-out.txt

exclusionInvisible.tex: invisibleModeDatfiles.anchor \
									$(SCRIPT_DIR)/gpis/na64-invis.gpi \
									$(INTERIM_DATFILES_DIR)/3sigma.dat \
									$(SCRIPT_DIR)/nData/mk-limits-17.08.017.dat \
									$(SCRIPT_DIR)/nData/mk-limits-06.10.017.dat \
									$(SCRIPT_DIR)/nData/mk-limits-05.05.019.dat \
									$(SCRIPT_DIR)/nData/mk-limits-05.05.019-corrections-to-23.03.023.dat \
									$(INTERIM_DATFILES_DIR)/g-2-combined.dat \
									$(SCRIPT_DIR)/nData/sng-additional.dat \
									$(INTERIM_DATFILES_DIR)/bezier-smooth-excl-inv.dat
	@echo -e "\033[36;1mPlotting \"Exclusion plot for invisible mode\"\033[0m"
	$(GNUPLOT) -e "datDir='$(INTERIM_DATFILES_DIR)';outFile='exclusionInvisible';scriptDir='$(SCRIPT_DIR)'" \
		$(SCRIPT_DIR)/gpis/na64-invis.gpi


exclusionInvisible_annihil.pdf: exclusionInvisible_annihil.tex
	@echo -e "\033[35;1mRendering plot $@\033[0m"
	$(LATEX) $^ > $@.latex-out.txt

exclusionInvisible_annihil.tex: invisibleModeDatfiles.anchor \
									$(SCRIPT_DIR)/gpis/na64-invis_annihil.gpi \
									$(INTERIM_DATFILES_DIR)/3sigma.dat \
									$(SCRIPT_DIR)/nData/mk-limits-17.08.017.dat \
									$(SCRIPT_DIR)/nData/mk-limits-06.10.017.dat \
									$(SCRIPT_DIR)/nData/mk-limits-05.05.019.dat \
									$(SCRIPT_DIR)/nData/mk-limits-05.05.019-corrections-to-23.03.023.dat \
									$(INTERIM_DATFILES_DIR)/g-2-combined.dat \
									$(SCRIPT_DIR)/nData/sng-additional.dat \
									$(INTERIM_DATFILES_DIR)/bezier-smooth-excl-inv.dat
	@echo -e "\033[36;1mPlotting \"Exclusion plot for invisible with annihilation\"\033[0m"
	$(GNUPLOT) -e "datDir='$(INTERIM_DATFILES_DIR)';outFile='exclusionInvisible_annihil';scriptDir='$(SCRIPT_DIR)'" \
		$(SCRIPT_DIR)/gpis/na64-invis_annihil.gpi


exclusionInvisible_2022B.pdf: exclusionInvisible_2022B.tex
	@echo -e "\033[35;1mRendering plot $@\033[0m"
	$(LATEX) $^ > $@.latex-out.txt

exclusionInvisible_2022B.tex: invisibleModeDatfiles.anchor \
									$(SCRIPT_DIR)/gpis/na64_invis_2022B.gpi \
									$(INTERIM_DATFILES_DIR)/3sigma.dat \
									$(SCRIPT_DIR)/nData/mk-limits-17.08.017.dat \
									$(SCRIPT_DIR)/nData/mk-limits-06.10.017.dat \
									$(SCRIPT_DIR)/nData/mk-limits-05.05.019.dat \
									$(SCRIPT_DIR)/nData/mk-limits-05.05.019-corrections-to-23.03.023.dat \
									$(INTERIM_DATFILES_DIR)/g-2-combined.dat \
									$(SCRIPT_DIR)/nData/sng-additional.dat \
									$(INTERIM_DATFILES_DIR)/bezier-smooth-excl-inv.dat
	@echo -e "\033[36;1mPlotting \"Exclusion plot for invisible mode / 2022B\"\033[0m"
	$(GNUPLOT) -e "datDir='$(INTERIM_DATFILES_DIR)';outFile='exclusionInvisible_2022B';scriptDir='$(SCRIPT_DIR)'" \
		$(SCRIPT_DIR)/gpis/na64_invis_2022B.gpi



invisibleModeDatfiles.anchor: $(DATASETS_INVISIBLE) \
							  $(SCRIPT_DIR)/invisibleMode.py | $(INTERIM_DATFILES_DIR)
	$(PREPROCESING_SCRIPT)  --wd $(INTERIM_DATFILES_DIR) \
		$(foreach src,$(DATASETS_INVISIBLE),--jsonSource $(src)) --noplot \
		--manual-spec $(SCRIPT_DIR)/invisibleMode.py
	@touch $@

$(INTERIM_DATFILES_DIR)/3sigma.dat: $(STATLINES_GENERATE) | $(INTERIM_DATFILES_DIR)
	$(STATLINES_GENERATE) $(STATLINES_NE) -o $@

$(INTERIM_DATFILES_DIR)/bezier-smooth-excl-inv.dat: $(SCRIPT_DIR)/gpis/dumpExclusionPlotInvisible.gpi \
													$(SCRIPT_DIR)/nData/mk-limits-05.05.019-corrections-to-23.03.023.dat
	@echo -e "\033[36;1mGenerating dump for Bezier smooth curve on NA64 invisible mode data\033[0m"
	$(GNUPLOT) -e "datDir='$(INTERIM_DATFILES_DIR)';outFile='exclusionInvisible';scriptDir='$(SCRIPT_DIR)'" $<

# 22.02.018

exclusionVisible-22.02.018.pdf: $(BUILD_DIR)/exclusionVisible-22.02.018.tex
	@echo -e "\033[35;1mRendering plot $@\033[0m"
	$(LATEX) $^ > $@.latex-out.txt

$(BUILD_DIR)/exclusionVisible-22.02.018.tex: $(SCRIPT_DIR)/gpis/excl-visible-22.02.018.gpi \
											 visibleModeDatfiles_1.anchor \
											 visibleModeDatfiles_2.anchor
	@echo -e "\033[36;1mPlotting \"Exclusion plot for visible mode / 22/02/2018\"\033[0m"
	$(GNUPLOT) -e "datDir='$(INTERIM_DATFILES_DIR)';outFile='$@';scriptDir='$(SCRIPT_DIR)'" $<

$(INTERIM_DATFILES_DIR)/g-2-combined.dat: invisibleModeDatfiles.anchor
	paste "$(INTERIM_DATFILES_DIR)/g-2-low_interp.dat" "$(INTERIM_DATFILES_DIR)/g-2-high_interp.dat" | awk '{print $$3,$$4,$$7,$$8}' > $@

#
# TLDRA (thermal light dark matter)
tldra.anchor:  $(TLDRA_DATFILES) $(MANSPEC_TLDRA) | $(INTERIM_DATFILES_DIR)
	@echo -e "\033[32;1mPreprocessing data files for \"thermal light dark matter\"\033[0m"
	$(foreach src,$(TLDRA_DATFILES), \
		$(MKDIR) -p $(patsubst $(SCRIPT_DIR)/nData/%.json,$(INTERIM_DATFILES_DIR)/%.dat-dir,$(src)) && \
		$(PREPROCESING_SCRIPT) --jsonSource $(src) \
			--wd $(patsubst $(SCRIPT_DIR)/nData/%.json,$(INTERIM_DATFILES_DIR)/%.dat-dir,$(src)) \
			--noplot --manual-spec $(patsubst $(SCRIPT_DIR)/nData/%.json,$(SCRIPT_DIR)/%.py,$(src));)
	@touch $@

new-stats-combined-june-2019.anchor: $(SCRIPT_DIR)/nData/new-stats-combined-june-2019.json $(SCRIPT_DIR)/stats-combined-june-2019.py
	@echo -e "\033[32;1mPreprocessing data files for \"June 2019, new stats\"\033[0m"
	$(MKDIR) -p $(INTERIM_DATFILES_DIR)/new-stats-combined-june-2019.dat-dir
	$(PREPROCESING_SCRIPT) --jsonSource $(SCRIPT_DIR)/nData/new-stats-combined-june-2019.json \
			--wd $(INTERIM_DATFILES_DIR)/new-stats-combined-june-2019.dat-dir --noplot \
			--manual-spec $(SCRIPT_DIR)/stats-combined-june-2019.py
	@touch $@
#$(BUILD_DIR)/tldra-%.tex: miniboone-extras.anchor tldra.anchor $(SCRIPT_DIR)/tldra-%.gpi $(SCRIPT_DIR)/nData/tldra-%.json
#	$(GNUPLOT) -e "datDir='$(patsubst $(BUILD_DIR)/tldra-%.tex,$(INTERIM_DATFILES_DIR)/tldra-%.dat-dir,$@)';outFile='$@';scriptDir='$(SCRIPT_DIR)'" $(word 3,$^)
#
#$(BUILD_DIR)/inh-tldra-%.tex: tldra.anchor $(SCRIPT_DIR)/inh-tldra-%.gpi $(INTERIM_DATFILES_DIR)/bezier-smooth-excl-inv.dat
#	$(GNUPLOT) -e "datDir='$(patsubst $(BUILD_DIR)/inh-tldra-%.tex,$(INTERIM_DATFILES_DIR)/tldra-21-1.dat-dir,$@)';outFile='$@';scriptDir='$(SCRIPT_DIR)'" $(word 2,$^)

$(SCRIPT_DIR)/tldra-19-2-%.gpi: $(SCRIPT_DIR)/gpis/tldra-19/heading.gpi \
							    $(SCRIPT_DIR)/gpis/tldra-19/body-%.gpi \
							    $(SCRIPT_DIR)/gpis/tldra-19/footer.gpi \
								new-stats-combined-june-2019.anchor \
								md-miniboone.anchor
	cat $(SCRIPT_DIR)/gpis/tldra-19/heading.gpi > $@
	cat $(SCRIPT_DIR)/gpis/tldra-19/body-$*.gpi >> $@
	cat $(SCRIPT_DIR)/gpis/tldra-19/footer.gpi >> $@

tldra-%.tex: miniboone-extras.anchor \
			  tldra.anchor \
			  $(SCRIPT_DIR)/tldra-%.gpi \
			  $(SCRIPT_DIR)/nData/tldra-%.json \
			  $(INTERIM_DATFILES_DIR)/bezier-smooth-excl-inv.dat \
			  invisibleModeDatfiles.anchor
	@echo -e "\033[36;1mPlotting thermal light dark matter for $@\033[0m"
	$(GNUPLOT) -e "datDir='$(patsubst tldra-%.tex,$(INTERIM_DATFILES_DIR)/tldra-%.dat-dir,$@)';outFile='$@';scriptDir='$(SCRIPT_DIR)';scriptDir='$(SCRIPT_DIR)'" $(word 3,$^)

inh-tldra-%.tex: tldra.anchor \
				 $(SCRIPT_DIR)/gpis/inh-tldra-%.gpi \
				 inh-tldra.anchor \
				 $(INTERIM_DATFILES_DIR)/bezier-smooth-excl-inv.dat \
				 new-stats-combined-june-2019.anchor
	@echo -e "\033[36;1mPlotting INHERITED thermal light dark matter for $@\033[0m"
	$(GNUPLOT) -e "datDir='$(patsubst inh-tldra-%.tex,$(INTERIM_DATFILES_DIR)/tldra-21-1.dat-dir,$@)';outFile='$@';scriptDir='$(SCRIPT_DIR)'" \
				  $(word 2,$^)

inh-tldra.anchor: $(SCRIPT_DIR)/nData/inh-tldra-20-1.json \
				  $(SCRIPT_DIR)/nData/inh-tldra-20-2.json
	@echo -e "\033[32;1mPreprocessing data files for \"INHERITED thermal light dark matter\"\033[0m"
	$(MKDIR) -p $(INTERIM_DATFILES_DIR)/tldra-20-1.dat-dir
	$(PREPROCESING_SCRIPT) --jsonSource $(SCRIPT_DIR)/nData/inh-tldra-20-1.json \
			--wd $(INTERIM_DATFILES_DIR)/tldra-20-1.dat-dir --noplot
	$(MKDIR) -p $(INTERIM_DATFILES_DIR)/tldra-20-2.dat-dir
	$(PREPROCESING_SCRIPT) --jsonSource $(SCRIPT_DIR)/nData/inh-tldra-20-2.json \
			--wd $(INTERIM_DATFILES_DIR)/tldra-20-2.dat-dir --noplot
	@touch $@

inh-tldra-%.pdf: inh-tldra-%.tex
	@echo -e "\033[35;1mRendering plot $@\033[0m"
	$(LATEX) $< > $@.latex-out.txt

tldra-%.pdf: tldra-%.tex \
			 md-miniboone.anchor
	@echo -e "\033[35;1mRendering plot $@\033[0m"
	$(LATEX) $< > $@.latex-out.txt

tldra: tldra-19-1.pdf \
	   tldra-19-2-alpha_0.1.pdf \
	   tldra-19-2-alpha_0.5.pdf \
	   tldra-2022B-annihil_alpha_0.1.pdf \
	   tldra-2022B-annihil_alpha_0.5.pdf \
	   tldra-21-1.pdf \
	   inh-tldra-20-1.pdf \
	   inh-tldra-20-2.pdf
#$(BUILD_DIR)/tldra-19-2-b.tex: $(BUILD_DIR)/tldra-19-2.tex miniboone-extras.anchor tldra.anchor $(SCRIPT_DIR)/tldra-19-2.gpi $(SCRIPT_DIR)/nData/tldra-19-2.json
#
#tldra-%.pdf: $(BUILD_DIR)/tldra-%.tex
#	$(LATEX) $<
#
#tldra: tldra-19-1.pdf tldra-19-2.pdf tldra-19-2-b.pdf tldra-21-1.pdf inh-tldra-20-1.pdf inh-tldra-20-2.pdf

#
# MiniBooNE extras (arxiv.org/pdf/1702.02688)
mnbe.pdf: mnbe.tex
	@echo -e "\033[35;1mRendering plot $@\033[0m"
	$(LATEX) $< > $@.latex-out.txt

# requires $(INTERIM_DATFILES_DIR)/tldra-19-2.dat-dir/BaBar_interp.dat
# 		 , $(INTERIM_DATFILES_DIR)/LSND_interp.dat
# 		 , E137-fermion
# which are produced by ...
mnbe.tex: $(SCRIPT_DIR)/gpis/mnbe.gpi \
		  miniboone-extras.anchor \
		  tldra.anchor \
		  inh-tldra.anchor \
		  invisibleModeDatfiles.anchor
	@echo -e "\033[36;1mPlotting \"MiniBooNE\"\033[0m"
	$(GNUPLOT) -e "datDir='$(INTERIM_DATFILES_DIR)';outFile='$@';scriptDir='$(SCRIPT_DIR)'" $<

miniboone-extras.anchor: $(SCRIPT_DIR)/nData/MiniBooNE-23-08-017.json \
									  $(MANSPEC_MINIBOONE) | $(INTERIM_DATFILES_DIR)
	@echo -e "\033[32;1mPreprocessing data file \"MiniBooNE 23/08/017\"\033[0m"
	mkdir -p $(INTERIM_DATFILES_DIR)/mnbe
	$(PREPROCESING_SCRIPT) --wd $(INTERIM_DATFILES_DIR)/mnbe \
		--jsonSource $< --noplot --manual-spec $(MANSPEC_MINIBOONE)
	@touch $@
#$(BUILD_DIR)/mnbe.tex: $(SCRIPT_DIR)/mnbe.gpi miniboone-extras.anchor tldra.anchor
#	$(GNUPLOT) -e "datDir='$(INTERIM_DATFILES_DIR)';outFile='$@';scriptDir='$(SCRIPT_DIR)'" $<
#
#miniboone-extras.anchor: $(SCRIPT_DIR)/nData/MiniBooNE-23-08-017.json $(MANSPEC_MINIBOONE) | $(INTERIM_DATFILES_DIR)
#	$(MKDIR) -p $(INTERIM_DATFILES_DIR)/mnbe


#
# ...more MiniBooNE data (arxiv.org/pdf/1807.06137)
md-miniboone.anchor: $(SCRIPT_DIR)/nData/dm-minib-fig25-a.json \
					 $(SCRIPT_DIR)/nData/dm-minib-fig26-a.json \
					 $(MANSPEC_MINIBOONE) | $(INTERIM_DATFILES_DIR)
	@echo -e "\033[32;1mPreprocessing data files \"MiniBooNE fig25,26\"\033[0m"
	$(MKDIR) -p $(INTERIM_DATFILES_DIR)/mnbe-dm.dat-dir
	$(PREPROCESING_SCRIPT) --jsonSource $(SCRIPT_DIR)/nData/dm-minib-fig25-a.json \
			--wd $(INTERIM_DATFILES_DIR)/mnbe-dm.dat-dir --noplot \
			--manual-spec $(MANSPEC_MINIBOONE)
	$(PREPROCESING_SCRIPT) --jsonSource $(SCRIPT_DIR)/nData/dm-minib-fig26-a.json \
			--wd $(INTERIM_DATFILES_DIR)/mnbe-dm.dat-dir --noplot \
			--manual-spec $(MANSPEC_MINIBOONE)
	@touch $@

#
# Pseudo-Goldstone Boson Rules
$(OUT_PGB).pdf: $(BUILD_DIR)/$(OUT_PGB).tex
	@echo -e "\033[35;1mRendering plot $@\033[0m"
	$(LATEX) $^ > $@.latex-out.txt

$(BUILD_DIR)/$(OUT_PGB).tex: pgb.anchor $(PGB_PLOT_SCRIPT) $(PGB_NA64_EXCLUSION_FILE)
	@echo -e "\033[36;1mPlotting \"Pseudo-Goldstone Boson\"\033[0m"
	$(GNUPLOT) -e "datDir='$(INTERIM_DATFILES_DIR)/pgb';outFile='$(OUT_PGB)';scriptDir='$(SCRIPT_DIR)'" $(PGB_PLOT_SCRIPT)

pgb.anchor: $(PGB_DATA) | $(INTERIM_DATFILES_DIR)/pgb
	@echo -e "\033[32;1mPreprocessing data files \"Pseudo-Goldstone Boson\"\033[0m"
	$(PREPROCESING_SCRIPT) --wd $(INTERIM_DATFILES_DIR)/pgb \
		--jsonSource $< --noplot --sort none --stage source #--manual-spec $(MANSPEC_MINIBOONE)

$(INTERIM_DATFILES_DIR)/pgb: $(INTERIM_DATFILES_DIR)
	$(MKDIR) $@

$(PGB_NA64_EXCLUSION_FILE): $(PGB_RECALC_SCRIPT) $(PGB_EXTRA_DEPS)
	$(PGB_RECALC_SCRIPT) $(PGB_NA64_EXCLUSION_FILE)

#
# Borexino combined (15 Nov 2017)

alphamu.anchor: $(ALPHAMU_JSDATA) $(ALPHAMU_MANSPEC) | $(INTERIM_DATFILES_DIR)
	@echo -e "\033[32;1mPreprocessing data file \"Borexino combined\"\033[0m"
	$(PREPROCESING_SCRIPT)  --wd $(INTERIM_DATFILES_DIR) \
			$(foreach src,$(ALPHAMU_JSDATA),--jsonSource $(src)) --noplot --manual-spec $(ALPHAMU_MANSPEC)
	@touch $@

borexino-combined.pdf: $(BUILD_DIR)/borexino-combined.tex
	@echo -e "\033[35;1mRendering plot $@\033[0m"
	$(LATEX) $^ > $@.latex-out.txt

$(BUILD_DIR)/borexino-combined.tex: $(SCRIPT_DIR)/gpis/borexino-combined.gpi invisibleModeDatfiles.anchor alphamu.anchor
	@echo -e "\033[36;1mPlotting \"Combined Borexino\"\033[0m"
	$(GNUPLOT) -e "datDir='$(INTERIM_DATFILES_DIR)';outFile='borexino-combined';scriptDir='$(SCRIPT_DIR)'" $<

#
# XENON1T plots

cs-el.pdf: cs-el.tex
	@echo -e "\033[35;1mRendering plot $@\033[0m"
	$(LATEX) $^ > $@.latex-out.txt

cs-el.tex: $(SCRIPT_DIR)/gpis/cross-section-el.gpi $(XENON1T_DATFILES) $(INTERIM_DATFILES_DIR)/bezier-smooth-excl-inv.dat
	@echo -e "\033[36;1mPlotting \"XENON1T\"\033[0m"
	$(GNUPLOT) -e "datDir='$(INTERIM_DATFILES_DIR)/xenon1t-cs';outFile='$@';scriptDir='$(SCRIPT_DIR)'" $<

$(INTERIM_DATFILES_DIR)/xenon1t-cs/%.dat: $(SCRIPT_DIR)/nData/XENON1T-el.json $(SCRIPT_DIR)/xenon1t-manspec.py
	@echo -e "\033[32;1mPreprocessing data file \"Xenon-1T\"\033[0m"
	$(MKDIR) -p $(INTERIM_DATFILES_DIR)/xenon1t-cs
	$(PREPROCESING_SCRIPT) --jsonSource $< \
		--manual-spec $(SCRIPT_DIR)/xenon1t-manspec.py \
		--wd $(INTERIM_DATFILES_DIR)/xenon1t-cs --noplot $*

#
# ALPs

$(INTERIM_DATFILES_DIR)/E141-Babette-interpolation.dat: $(SCRIPT_DIR)/nData/E141-Babette.dat
	@echo -e "\033[32;1m(Custom) preprocessing data file \"E141 interpolation from Babette\"\033[0m"
	python $(SCRIPT_DIR)/py/alps-e141.py $< interpolation > $@


$(INTERIM_DATFILES_DIR)/alps-019-manual.dat: $(SCRIPT_DIR)/nData/alps-019-manual.json
	@echo -e "\033[32;1mPreprocessing data file \"Manual ALPs\"\033[0m"
	$(PREPROCESING_SCRIPT) --jsonSource $< \
		--wd $(INTERIM_DATFILES_DIR) --noplot alps-019-manual

$(INTERIM_DATFILES_DIR)/NuCal-2020_interp.dat: $(SCRIPT_DIR)/nData/NuCAL-CHARM-2020-update.json \
												$(SCRIPT_DIR)/alps-manspec.py
	@echo -e "\033[32;1mPreprocessing data file \"NuCal 2020\"\033[0m"
	$(PREPROCESING_SCRIPT) --jsonSource $< \
		--manual-spec $(SCRIPT_DIR)/alps-manspec.py \
		--wd $(INTERIM_DATFILES_DIR) --noplot NuCal-2020

$(INTERIM_DATFILES_DIR)/CHARM-2020_interp.dat: $(SCRIPT_DIR)/nData/NuCAL-CHARM-2020-update.json \
												$(SCRIPT_DIR)/alps-manspec.py
	@echo -e "\033[32;1mPreprocessing data file \"CHARM 2020\"\033[0m"
	$(PREPROCESING_SCRIPT) --jsonSource $< \
		--manual-spec $(SCRIPT_DIR)/alps-manspec.py \
		--wd $(INTERIM_DATFILES_DIR) --noplot CHARM-2020

alps.tex: $(SCRIPT_DIR)/gpis/alps.gpi \
		  $(INTERIM_DATFILES_DIR)/alps-019-manual.dat \
		  $(SCRIPT_DIR)/nData/alps-020-manual.dat \
		  $(INTERIM_DATFILES_DIR)/E141-Babette-interpolation.dat \
		  $(INTERIM_DATFILES_DIR)/NuCal-2020_interp.dat \
		  $(INTERIM_DATFILES_DIR)/CHARM-2020_interp.dat
	@echo -e "\033[36;1mPlotting \"ALPs\"\033[0m"
	$(GNUPLOT) -e "datDir='$(INTERIM_DATFILES_DIR)/';outFile='$@';scriptDir='$(SCRIPT_DIR)'" $<

alps.pdf: alps.tex
	@echo -e "\033[35;1mRendering plot $@\033[0m"
	$(LATEX) $^ > $@.latex-out.txt

#
# Interim data files directory
$(INTERIM_DATFILES_DIR):
	$(MKDIR) -p $(INTERIM_DATFILES_DIR)

#
# Numerical data
# 	y-m_chi_alpha_D-0.1_EOT-4.3e10
table.%.dat: $(SCRIPT_DIR)/gpis/table-rndr/%.gpi $(INTERIM_DATFILES_DIR)/bezier-smooth-excl-inv.dat
	@echo -e "\033[36;1mGenerating $@ dump\"\033[0m"
	$(GNUPLOT) -e "datDir='$(INTERIM_DATFILES_DIR)';outFile='$@';scriptDir='$(SCRIPT_DIR)'" $<
	$(GREP) -v "^#.*" $(INTERIM_DATFILES_DIR)/$@ \
		| $(GREP) "i$$" \
		| $(AWK) '{ printf "%.3e\t%.3e\n", $$1, $$2 }' > $@

clean:
	@rm -vrf $(INTERIM_DATFILES_DIR)
	@rm -vf visibleModeDatfiles_1.anchor
	@rm -vf visibleModeDatfiles_2.anchor
	@rm -vf visibleModeDatfiles_3.anchor
	@rm -vf invisibleModeDatfiles.anchor
	@rm -vf inh-tldra.anchor
	@rm -vf tldra.anchor
	@rm -vf miniboone-extras.anchor
	@rm -vf exclusionVisible-gnuplot-inclusions.tmp \
			exclusionVisible.log \
			exclusionVisible.pdf \
			exclusionVisible.tex \
			exclusionVisible.tuc

.PHONY: all clean tldra tables

#$(INTERIM_DATFILES_DIR)/*: $(SCRIPT_DIR)/nData/wpd_plot_data.json $(SCRIPT_DIR)/nData/wpd_plot_data_k2_zoom.json


manualParameters = {
    "xenon1t-optimistic" : {
        'centerX' : 1,
        'centerY' : 4e-30,
        'interpolationMethod' : 'UnivariateSpline',
        'smoothFactor' : .5,
    },
    "xenon1t-pessimistic" : {
        'centerX' : 1,
        'centerY' : 4e-30,
        'interpolationMethod' : 'UnivariateSpline',
        'smoothFactor' : .5,
    },
}
